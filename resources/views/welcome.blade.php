<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <!-- csrf-token se usa por temas de seguridas  -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <!-- el contenedor div con id app es donde se ejecurata la 
            app de Vue como lo especifica app.js -->
        <div id="app">
            <!-- renderiza los componentes llamados por las rutas definidas -->
            <router-view></router-view>
        </div>
       <!-- importa el app.js donde se tiene la configuracion de Vue -->
       <script src=" {{ asset('js/app.js') }} " ></script> 
    </body>
</html>
