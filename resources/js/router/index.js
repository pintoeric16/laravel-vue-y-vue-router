// importar Vue y Vue Router 

import Vue from 'vue';
import Router from 'vue-router';

// importa los componentes 
import UserComponent from '../components/UserComponent.vue';
import ProfileComponent from '../components/ProfileComponent.vue';
import AdminComponent from '../components/AdminComponent.vue';

// especifica a Vue que usara Vue Router 

Vue.use(Router);

// crea a instancia de Vue Router 

export default new Router({
    // previene el # en la url 
    mode: 'history', 
    // array de rutas 
    routes: [
        {
            // indica la raiz de la ruta 
            path: '/',
            name: 'user',
            // y el componente que se renderiza 
            component: UserComponent,
            // children indica una ruta hija
            // que va a estar contenida en User como 
            // se muestra en la imagen anterios 
            children: [
                {
                    path: 'profile',
                    component: ProfileComponent
                }
            ]
        },
        {   // indica la ruta admin 
            path: '/admin',
            name: 'admin',
            // y el componente que se renderiza 
            component: AdminComponent,
        }
    ]

});