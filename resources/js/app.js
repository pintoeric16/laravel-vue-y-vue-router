
import router from './router/index'

require('./bootstrap');

window.Vue = require('vue');


Vue.component('user-component', require('./components/UserComponent.vue').default);


const app = new Vue({
    router,
}).$mount('#app');
